package lab.codetorium.carikerja.Screen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import lab.codetorium.carikerja.Screen.Helper.RecyclerViewActivity
import lab.codetorium.carikerja.UI.Item.State.SearchState
import lab.codetorium.carikerja.UI.Item.State.SingleTextItemState

class MultitextSearchScreen : RecyclerViewActivity() {
    lateinit var title: String
    lateinit var listOfChoice: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent?.extras?.let {
            title = it.getString(SEARCH_TITLE_ARG, "")
            listOfChoice = it.getStringArrayList(KEY_LIST_ARG) ?: arrayListOf()
        }

        supportActionBar?.title = title
        itemList.clear()
        itemList.add(SearchState())
        listOfChoice.takeIf { it.isNotEmpty() }?.forEach {
            itemList.add(SingleTextItemState(it) { selectedChoice ->
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra(SEARCH_TITLE_ARG, title)
                    putExtra(SELECTED_VALUE_ARG, selectedChoice)
                })
                finish()
            })
        }
        updateView()
    }

    override fun onSwipeAction() {

    }

    override fun getItemDecoration(): RecyclerView.ItemDecoration? = null
    override fun isSwipeAble(): Boolean = false

    companion object {
        const val SEARCH_TITLE_ARG = "search_title_arg"
        const val KEY_LIST_ARG = "key_list_arg"
        const val SELECTED_VALUE_ARG = "selected_value_arg"

        const val REQ_CODE_MULTITEXT_SEARCH_SCREEN = 1
    }
}