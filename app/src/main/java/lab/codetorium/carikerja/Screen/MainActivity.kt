package lab.codetorium.carikerja.Screen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import lab.codetorium.carikerja.Api.Api
import lab.codetorium.carikerja.Api.Model.Job
import lab.codetorium.carikerja.Api.Service.AvailableJobService
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.UI.Adapter.GenericAdapter
import lab.codetorium.carikerja.UI.Helper.EndlessScrollListener
import lab.codetorium.carikerja.UI.Helper.TransparentDivider
import lab.codetorium.carikerja.UI.Item.State.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private val itemList: MutableList<ItemState> = arrayListOf()
    private var jobList = arrayListOf<ItemState>()
    private val rvAdapter = GenericAdapter(listOf())

    private var page = 1
    private val perPageItem = 5
    private var isLastPage = false
    private var isLoading = true
    set(value) {
        field = value
        setLoadingView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadData()

        val linearLayoutManager = LinearLayoutManager(this@MainActivity,
                LinearLayoutManager.VERTICAL,
                false)

        recyclerView.adapter = rvAdapter
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.addItemDecoration(TransparentDivider())
        recyclerView.addOnScrollListener(object: EndlessScrollListener(linearLayoutManager) {
            override fun loadMoreItems() {
                page += 1
                loadData(page)
            }
            override fun isLastPage(): Boolean = isLastPage
            override fun isLoading(): Boolean = isLoading
        })

        swipeRefreshParent.setOnRefreshListener {
            reloadData()
        }
    }

    private fun reloadData() {
        page = 1
        loadData()
    }

    private fun loadData(page: Int = 1) {
        isLoading = true
        Api.instance.create(AvailableJobService::class.java)
                .retrieveJobs(page, perPageItem)
                .enqueue(object : Callback<ArrayList<Job>> {
                    override fun onFailure(call: Call<ArrayList<Job>>, t: Throwable) {
                        isLoading = false
                        Log.d("RESULT", "FAILED " + t.message)
                    }

                    override fun onResponse(call: Call<ArrayList<Job>>, response: Response<ArrayList<Job>>) {
                        isLoading = false
                        swipeRefreshParent.isRefreshing = false
                        if (response.isSuccessful) {
                            val jobs = response.body()
                            jobs?.let { result ->
                                val responseJobs: ArrayList<ItemState> = ArrayList(result.map {
                                    JobItemState(it)
                                })

                                if (page == 1) {
                                    jobList = responseJobs
                                } else {
                                    jobList.addAll(responseJobs)
                                }

                                isLastPage = responseJobs.size < perPageItem

                                regenerateItem()
                            } ?: Toast.makeText(this@MainActivity,
                                    response.errorBody().toString(),
                                    Toast.LENGTH_SHORT).show()
                        }
                    }
                })
    }

    fun regenerateItem() {
        itemList.clear()
        itemList.add(HomeSearchState())
        itemList.addAll(jobList)

        rvAdapter.updateDataSet(itemList)
        rvAdapter.notifyDataSetChanged()
    }

    private fun setLoadingView() {
        if (isLoading) {
            jobList.add(LoadingState())
            regenerateItem()
        } else {
            if (jobList.last() is LoadingState) {
                jobList.removeAt(jobList.lastIndex)
                regenerateItem()
            }
        }
    }
}

