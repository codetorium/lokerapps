package lab.codetorium.carikerja.Screen.Helper

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.UI.Adapter.GenericAdapter
import lab.codetorium.carikerja.UI.Item.State.ItemState

@SuppressLint("Registered")
abstract class RecyclerViewActivity : AppCompatActivity() {
    val rvAdapter = GenericAdapter(listOf())
    val itemList: MutableList<ItemState> = arrayListOf()
    val result = MutableLiveData<CustomActivityResult>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.setBackgroundColor(ContextCompat.getColor(this@RecyclerViewActivity, R.color.white))
        recyclerView.adapter = rvAdapter

        getItemDecoration()?.let {
            recyclerView.addItemDecoration(it)
        }
        recyclerView.layoutManager =  LinearLayoutManager(this@RecyclerViewActivity,
                LinearLayoutManager.VERTICAL,
                false)

        swipeRefreshParent.isEnabled = isSwipeAble()
        swipeRefreshParent.setOnRefreshListener {
            onSwipeAction()
        }
    }

    abstract fun onSwipeAction()
    abstract fun getItemDecoration() : RecyclerView.ItemDecoration?
    abstract fun isSwipeAble() : Boolean

    fun updateView() {
        rvAdapter.updateDataSet(itemList)
        rvAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        result.value = CustomActivityResult(requestCode, resultCode, data)
    }

    fun subscribeToActivityResult(functionToCall: (CustomActivityResult?) -> Unit) {
        result.removeObserver {}
        result.observeForever {
            functionToCall.invoke(it)
        }
    }
}