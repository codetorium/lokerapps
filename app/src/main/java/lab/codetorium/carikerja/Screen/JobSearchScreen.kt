package lab.codetorium.carikerja.Screen

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import lab.codetorium.carikerja.Api.Api
import lab.codetorium.carikerja.Api.Model.Job
import lab.codetorium.carikerja.Api.Service.AvailableJobService
import lab.codetorium.carikerja.Screen.Helper.RecyclerViewActivity
import lab.codetorium.carikerja.UI.Helper.TransparentDivider
import lab.codetorium.carikerja.UI.Item.State.DividerState
import lab.codetorium.carikerja.UI.Item.State.JobItemState
import lab.codetorium.carikerja.UI.Item.State.SearchWithFilterState
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobSearchScreen : RecyclerViewActivity() {
    var filterMap: Map<String, Any> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemList.clear()
        itemList.add(SearchWithFilterState { query ->
            Api.instance.create(AvailableJobService::class.java)
                    .searchJob(query)
                    .enqueue(object: Callback<ArrayList<Job>> {
                        override fun onFailure(call: Call<ArrayList<Job>>, t: Throwable) {

                        }

                        override fun onResponse(call: Call<ArrayList<Job>>, response: Response<ArrayList<Job>>) {
                            response.body()?.let {
                                itemList.addAll(itemList.indexOfFirst { state -> state is DividerState } + 1, it.map { job ->
                                    JobItemState(job)
                                })
                            }
                        }
                    })
        })
        itemList.add(DividerState())
        updateView()
    }

    override fun onSwipeAction() {}
    override fun getItemDecoration(): RecyclerView.ItemDecoration? = TransparentDivider()
    override fun isSwipeAble(): Boolean = false
}