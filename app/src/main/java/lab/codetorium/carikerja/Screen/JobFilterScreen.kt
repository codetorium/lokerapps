package lab.codetorium.carikerja.Screen

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import lab.codetorium.carikerja.Api.Api
import lab.codetorium.carikerja.Api.Model.JobFilter
import lab.codetorium.carikerja.Api.Service.FilterService
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.Screen.Helper.RecyclerViewActivity
import lab.codetorium.carikerja.Screen.MultitextSearchScreen.Companion.SEARCH_TITLE_ARG
import lab.codetorium.carikerja.Screen.MultitextSearchScreen.Companion.SELECTED_VALUE_ARG
import lab.codetorium.carikerja.UI.Item.State.BlockTextWithTitleState
import lab.codetorium.carikerja.UI.Item.State.ButtonState
import lab.codetorium.carikerja.UI.Item.State.DividerState
import lab.codetorium.carikerja.UI.Item.State.FilterState.FilterUtils
import lab.codetorium.carikerja.UI.Item.State.LoadingState
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobFilterScreen : RecyclerViewActivity() {
    var states: ArrayList<JobFilter>? = arrayListOf()

    fun refreshView() {
        itemList.clear()
        itemList.add(BlockTextWithTitleState(getString(R.string.filter_screen_title),
                getString(R.string.filter_screen_description)))
        itemList.add(DividerState())
        itemList.addAll(states?.map {
            FilterUtils.getWidgetFilter(it, this@JobFilterScreen)
        } ?: listOf(LoadingState()))
        itemList.add(ButtonState("Terapkan") {
            Toast.makeText(this@JobFilterScreen,
                    "Applied",
                    Toast.LENGTH_LONG).show()
        })

        updateView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        refreshView()
        Api.instance.create(FilterService::class.java)
                .retrieveFilter()
                .enqueue(object : Callback<ArrayList<JobFilter>> {
                    override fun onFailure(call: Call<ArrayList<JobFilter>>, t: Throwable) {

                    }

                    override fun onResponse(call: Call<ArrayList<JobFilter>>, response: Response<ArrayList<JobFilter>>) {
                        states = response.body()
                        refreshView()
                    }
                })
    }

    override fun onSwipeAction() {}
    override fun getItemDecoration(): RecyclerView.ItemDecoration? = null
    override fun isSwipeAble(): Boolean = false

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.run {
            states?.filter { it.value == getStringExtra(SEARCH_TITLE_ARG) }?.first {
                it.inputtedValue = getStringExtra(SELECTED_VALUE_ARG)
                true
            }

            refreshView()
        }
    }
}