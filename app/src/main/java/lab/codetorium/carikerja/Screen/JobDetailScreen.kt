package lab.codetorium.carikerja.Screen

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import lab.codetorium.carikerja.Api.Model.Job
import lab.codetorium.carikerja.Screen.Helper.RecyclerViewActivity
import lab.codetorium.carikerja.UI.Item.State.BlockTextWithTitleState
import lab.codetorium.carikerja.UI.Item.State.ButtonState
import lab.codetorium.carikerja.UI.Item.State.DividerState
import lab.codetorium.carikerja.UI.Item.State.ImageState

class JobDetailScreen : RecyclerViewActivity() {
    private var job: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        job = intent.getSerializableExtra(JOB_ARG) as? Job

        job?.run {
            itemList.clear()
            itemList.add(ImageState(imageUrl))
            itemList.add(DividerState())
            itemList.add(BlockTextWithTitleState("Deskripsi", description))
            itemList.add(DividerState())
            itemList.add(BlockTextWithTitleState("Persyaratan", description))
            itemList.add(DividerState())
            itemList.add(BlockTextWithTitleState("Lokasi", location))
            itemList.add(DividerState())
            itemList.add(BlockTextWithTitleState("Posisi", position.jobTitle))
            itemList.add(DividerState())
            itemList.add(ButtonState("Daftar"))
            updateView()
        }
    }

    override fun onSwipeAction() {

    }

    override fun getItemDecoration(): RecyclerView.ItemDecoration? = null
    override fun isSwipeAble(): Boolean = false

    companion object {
        const val JOB_ARG = "job"
    }
}
