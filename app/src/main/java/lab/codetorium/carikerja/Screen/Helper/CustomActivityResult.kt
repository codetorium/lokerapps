package lab.codetorium.carikerja.Screen.Helper

import android.content.Intent

data class CustomActivityResult(var requestCode: Int,
                                var resultCode: Int,
                                var data: Intent?)