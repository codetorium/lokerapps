package lab.codetorium.carikerja

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import android.util.DisplayMetrics
import android.view.ViewGroup
import java.security.AccessController.getContext

fun Context.inflate(layoutId: Int, parent: ViewGroup, attachToRoot: Boolean): View {
    return LayoutInflater.from(this).inflate(layoutId, parent, attachToRoot)
}

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Int.toPx(context: Context): Int {
    val displayMetrics = context.getResources().getDisplayMetrics()
    return Math.round(this * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}