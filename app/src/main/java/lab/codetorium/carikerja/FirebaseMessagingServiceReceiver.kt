package lab.codetorium.carikerja

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseMessagingServiceReceiver : FirebaseMessagingService() {
    override fun onMessageReceived(p0: RemoteMessage?) {
        Log.d("Testing Notification", p0?.notification.toString())
    }

    override fun onNewToken(p0: String?) {
        Log.d("Testing Notification", p0 ?: "")
        super.onNewToken(p0)
    }
}