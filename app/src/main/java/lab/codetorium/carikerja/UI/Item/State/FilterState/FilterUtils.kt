package lab.codetorium.carikerja.UI.Item.State.FilterState

import android.content.Context
import lab.codetorium.carikerja.Api.Model.JobFilter
import lab.codetorium.carikerja.UI.Item.State.ItemState

object FilterUtils {
    const val TEXT = "TEXT"
    const val NUMBER = "NUMBER"
    const val BOOLEAN = "BOOLEAN"
    const val MULTITEXT = "MULTITEXT"

    fun getWidgetFilter(filter: JobFilter, parentActivity: Context? = null): ItemState {
        return when (filter.valueType) {
            NUMBER -> NumberFilterState(filter)
            BOOLEAN -> BooleanFilterState(filter)
            MULTITEXT -> MultiTextFilterState(filter, parentActivity)
            else -> TextFilterState()
        }
    }
}