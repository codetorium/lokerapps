package lab.codetorium.carikerja.UI.Item.State.FilterState

import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.layout_text_with_switch.view.*
import lab.codetorium.carikerja.Api.Model.JobFilter
import lab.codetorium.carikerja.UI.Item.State.ItemState

class BooleanFilterState(var filter: JobFilter) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {
            tvSwitchTitle.text = filter.value
            tvSwitchHint.text = filter.hint

            tvSwitchHint.visibility = if (filter.hint.isNullOrBlank()) {
                GONE
            } else {
                VISIBLE
            }
        }
    }

    override fun getType(): Int = FILTER_BOOLEAN_ITEM
    override fun getValue(): Any? {
        return null
    }
}