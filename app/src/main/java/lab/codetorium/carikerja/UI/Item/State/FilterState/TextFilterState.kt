package lab.codetorium.carikerja.UI.Item.State.FilterState

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.layout_text_with_edit_text.view.*
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.UI.Item.State.ItemState

class TextFilterState : ItemState() {
    var view: View? = null

    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            view = this

            tvTitle.text = context.getString(R.string.filter_keyword)
        }
    }

    override fun getValue(): Any? {
        view?.etJobKeyword?.let {
            return it.text
        } ?: return null
    }

    override fun getType(): Int = FILTER_TEXT_ITEM
}