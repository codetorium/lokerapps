package lab.codetorium.carikerja.UI.Item.State

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH
import kotlinx.android.synthetic.main.layout_search_with_filter.view.*
import lab.codetorium.carikerja.Screen.JobFilterScreen

class SearchWithFilterState(val onSearchListener: (String) -> Unit) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {
            etSearch.setOnEditorActionListener { v, actionId, _ ->
                if (actionId == IME_ACTION_SEARCH) {
                    onSearchListener.invoke(v.text.toString())
                }
                false
            }

            ibFilter.setOnClickListener {
                context.startActivity(Intent(context, JobFilterScreen::class.java))
            }
        }
    }

    override fun getType(): Int = SEARCH_FILTER_ITEM
}