package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_single_text.view.*

class SingleTextItemState(val content: String,
                          val onClickListener: (String) -> Unit) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {
            tvContent.text = content

            setOnClickListener {
                onClickListener.invoke(content)
            }
        }
    }

    override fun getType(): Int = SINGLE_TEXT_ITEM
}