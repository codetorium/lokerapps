package lab.codetorium.carikerja.UI.Item.State

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.layout_custom_toolbar.view.*
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.Screen.JobSearchScreen
import lab.codetorium.carikerja.toast

class HomeSearchState : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            tvSearch.setOnClickListener {
                context.startActivity(Intent(context, JobSearchScreen::class.java))
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.layout_custom_toolbar
    override fun getType(): Int = SEARCH_HEADER_ITEM
}