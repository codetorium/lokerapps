package lab.codetorium.carikerja.UI.Item.State.FilterState

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_text_with_spinner.view.*
import lab.codetorium.carikerja.Api.Model.JobFilter
import lab.codetorium.carikerja.Screen.Helper.RecyclerViewActivity
import lab.codetorium.carikerja.Screen.MultitextSearchScreen
import lab.codetorium.carikerja.Screen.MultitextSearchScreen.Companion.REQ_CODE_MULTITEXT_SEARCH_SCREEN
import lab.codetorium.carikerja.UI.Item.State.ItemState

class MultiTextFilterState(var filter: JobFilter,
                           var parentActivity: Context?) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            tvTitle.text = filter.value
            tvDescription.text = filter.inputtedValue ?: "Input ${filter.value} terlebih dahulu"
            setOnClickListener {
                (parentActivity as? RecyclerViewActivity)?.run {
                    startActivityForResult(Intent(this, MultitextSearchScreen::class.java).apply {
                        putExtra(MultitextSearchScreen.KEY_LIST_ARG, filter.suggestedValue as ArrayList<String>)
                        putExtra(MultitextSearchScreen.SEARCH_TITLE_ARG, filter.value)
                    }, REQ_CODE_MULTITEXT_SEARCH_SCREEN)
                }
            }
        }
    }

    override fun getType(): Int = FILTER_MULTITEXT
}