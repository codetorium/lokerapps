package lab.codetorium.carikerja.UI.Item.State

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_job_item.view.*
import lab.codetorium.carikerja.Api.Model.Job
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.Screen.JobDetailScreen
import lab.codetorium.carikerja.Screen.JobDetailScreen.Companion.JOB_ARG

class JobItemState(var model: Job) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            tvJobTitle.text = model.title
            tvDescription.text = model.description
            tvAvailableCount.text = context.getString(R.string.text_num_of_position, model.position.supportedField.size)
            Glide.with(context)
                    .load(model.imageUrl)
                    .into(ivJob)

            setOnClickListener {
                context.startActivity(Intent(context, JobDetailScreen::class.java).apply {
                    putExtra(JOB_ARG, model)
                })
            }
        }
    }

    override fun getType(): Int = JOB_ITEM
}