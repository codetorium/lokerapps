package lab.codetorium.carikerja.UI.Helper

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import lab.codetorium.carikerja.toPx

class TransparentDivider : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) != 0) {
            outRect.set(0, 8.toPx(view.context), 0, 0)
        }
    }
}