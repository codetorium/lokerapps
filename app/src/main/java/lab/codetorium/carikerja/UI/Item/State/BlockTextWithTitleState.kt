package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.layout_text_with_title.view.*

class BlockTextWithTitleState(val title: String, val content: String) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            tvTitle.text = title
            tvContent.text = content
        }
    }

    override fun getLayoutId(): Int = 0
    override fun getType(): Int = TEXT_WITH_TITLE_ITEM
}