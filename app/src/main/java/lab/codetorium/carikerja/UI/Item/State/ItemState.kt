package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView

open class ItemState {

    open fun bind(viewHolder: RecyclerView.ViewHolder) {}
    open fun getLayoutId() : Int = 0
    open fun getType(): Int = 0
    open fun getValue(): Any? = null

    companion object {
        const val JOB_ITEM = 0
        const val SEARCH_HEADER_ITEM = 1
        const val BUTTON = 2
        const val LOADING_ITEM = 3
        const val SEARCH_FILTER_ITEM = 4
        const val IMAGE_ITEM = 5
        const val TEXT_WITH_TITLE_ITEM = 6
        const val DIVIDER_ITEM = 7

        // FILTER ITEM
        const val FILTER_TEXT_ITEM = 8
        const val FILTER_BOOLEAN_ITEM = 9
        const val FILTER_NUMBER_ITEM = 10
        const val FILTER_MULTITEXT = 11

        const val SEARCH_ITEM = 12
        const val SINGLE_TEXT_ITEM = 13
    }
}