package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView

class SearchState : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {

        }
    }

    override fun getType(): Int = SEARCH_ITEM
}