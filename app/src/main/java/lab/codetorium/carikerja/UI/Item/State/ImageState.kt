package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import lab.codetorium.carikerja.toPx

class ImageState(var imgUrl: String) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView as ImageView) {
            layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

            setPadding(16.toPx(context),
                    16.toPx(context),
                    16.toPx(context),
                    16.toPx(context))

            Glide.with(context)
                    .load(imgUrl)
                    .into(this)
        }
    }

    override fun getLayoutId(): Int = 0
    override fun getType(): Int = IMAGE_ITEM
}