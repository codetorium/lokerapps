package lab.codetorium.carikerja.UI.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.UI.Item.State.*
import lab.codetorium.carikerja.UI.Item.State.FilterState.TextFilterState
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.BUTTON
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.DIVIDER_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.FILTER_BOOLEAN_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.FILTER_MULTITEXT
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.FILTER_NUMBER_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.FILTER_TEXT_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.IMAGE_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.LOADING_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.SEARCH_FILTER_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.SEARCH_HEADER_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.SEARCH_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.SINGLE_TEXT_ITEM
import lab.codetorium.carikerja.UI.Item.State.ItemState.Companion.TEXT_WITH_TITLE_ITEM
import lab.codetorium.carikerja.inflate

class GenericAdapter(private var itemStateList: List<ItemState>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, type: Int): RecyclerView.ViewHolder {
        return when(type) {
            BUTTON -> GenericViewHolder(Button(parent.context))
            SEARCH_HEADER_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_custom_toolbar))
            LOADING_ITEM -> GenericViewHolder(ProgressBar(parent.context))
            IMAGE_ITEM -> GenericViewHolder(ImageView(parent.context))
            TEXT_WITH_TITLE_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_text_with_title))
            SEARCH_FILTER_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_search_with_filter))
            DIVIDER_ITEM -> GenericViewHolder(LinearLayout(parent.context))

            // FILTER ITEM
            FILTER_TEXT_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_text_with_edit_text))
            FILTER_BOOLEAN_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_text_with_switch))
            FILTER_NUMBER_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_text_with_number))
            FILTER_MULTITEXT -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_text_with_spinner))

            SEARCH_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_search_item))
            SINGLE_TEXT_ITEM -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_single_text))
            else -> GenericViewHolder(getViewFromLayout(parent, R.layout.layout_job_item))
        }
    }

    override fun getItemCount(): Int = itemStateList.size
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        itemStateList[position].bind(viewHolder)
    }

    override fun getItemViewType(position: Int): Int {
        return itemStateList[position].getType()
    }

    private fun getViewFromLayout(parent: ViewGroup, layout: Int): View {
        return parent.context.inflate(layout,
                parent,
                false)
    }

    fun updateDataSet(newList : List<ItemState>) {
        itemStateList = newList
        notifyDataSetChanged()
    }

    class GenericViewHolder(var view: View): RecyclerView.ViewHolder(view)
}