package lab.codetorium.carikerja.UI.Item.State

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ProgressBar

class LoadingState(var width: Int? = null,
                   var height: Int? = null) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        (viewHolder.itemView as ProgressBar).let {
            it.layoutParams = RecyclerView.LayoutParams(width ?: MATCH_PARENT, height ?: WRAP_CONTENT)
            it.isIndeterminate = true
        }
    }

    override fun getType(): Int = LOADING_ITEM
}