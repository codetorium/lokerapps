package lab.codetorium.carikerja.UI.Item.State.FilterState

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.layout_text_with_number.view.*
import lab.codetorium.carikerja.Api.Model.JobFilter
import lab.codetorium.carikerja.UI.Item.State.ItemState

class NumberFilterState(var filter: JobFilter) : ItemState() {
    var view: View? = null

    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with (viewHolder.itemView) {
            view = this
            tvNumberTitle.text = filter.value
            etNumber.hint = filter.hint
        }
    }

    override fun getValue(): Any? {
        view?.etNumber?.let {
            return it.text
        } ?: return null
    }

    override fun getType(): Int = FILTER_NUMBER_ITEM
}