package lab.codetorium.carikerja.UI.Item.State

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.toPx

class ButtonState(var buttonText: String = "",
                  var clickListener: (() -> Unit)? = null) : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView as Button) {
            layoutParams = RecyclerView.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                setMargins(16.toPx(context),
                        12.toPx(context),
                        16.toPx(context),
                        12.toPx(context))
            }
            text = buttonText
            setOnClickListener {
                clickListener?.invoke()
            }
            setBackgroundColor(ContextCompat.getColor(context, R.color.blue))
            setTextColor(ContextCompat.getColor(context, R.color.white))
        }
    }

    override fun getLayoutId(): Int = 0
    override fun getType(): Int = BUTTON
}