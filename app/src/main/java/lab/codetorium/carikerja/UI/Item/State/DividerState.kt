package lab.codetorium.carikerja.UI.Item.State

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL
import lab.codetorium.carikerja.R
import lab.codetorium.carikerja.toPx

class DividerState : ItemState() {
    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        (viewHolder.itemView as? LinearLayout)?.run {
            layoutParams = RecyclerView.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            orientation = VERTICAL

            removeAllViews()
            addView(View(context).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 1.toPx(context))
                setBackgroundColor(ContextCompat.getColor(context, R.color.silver))
                alpha = 0.5f
            })

            addView(View(context).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 12.toPx(context))
                setBackgroundColor(ContextCompat.getColor(context, R.color.cloud))
            })

            addView(View(context).apply {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, 1.toPx(context))
                setBackgroundColor(ContextCompat.getColor(context, R.color.silver))
                alpha = 0.5f
            })
        }
    }

    override fun getType(): Int = DIVIDER_ITEM
}