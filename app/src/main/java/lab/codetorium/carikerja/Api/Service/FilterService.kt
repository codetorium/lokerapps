package lab.codetorium.carikerja.Api.Service

import lab.codetorium.carikerja.Api.Model.JobFilter
import retrofit2.Call
import retrofit2.http.GET

interface FilterService {
    @GET("filter")
    fun retrieveFilter(): Call<ArrayList<JobFilter>>
}