package lab.codetorium.carikerja.Api.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class JobPosition : Serializable {
    @SerializedName("job_title")
    var jobTitle : String = ""
    @SerializedName("supported_field")
    var supportedField: List<String> = listOf()
}