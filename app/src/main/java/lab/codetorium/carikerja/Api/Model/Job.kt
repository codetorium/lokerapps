package lab.codetorium.carikerja.Api.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Job : Serializable {
    @SerializedName("job_position")
    var position: JobPosition = JobPosition()
    @SerializedName("is_bumn")
    var isBumn: Boolean = false
    @SerializedName("is_swasta")
    var isSwasta: Boolean = false
    @SerializedName("image_url")
    var imageUrl: String = ""

    var location: String = ""
    var latitude: Double? = null
    var longitude: Double? = null
    var id: Long = 0L
    var title: String = ""
    var description: String = ""
}