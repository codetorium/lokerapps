package lab.codetorium.carikerja.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Api {
    companion object {
        const val OBJECT_PER_PAGE = 10
        const val BASE_URL = "http://10.0.3.2:3000"

        val instance: Retrofit by lazy {
            Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
    }
}