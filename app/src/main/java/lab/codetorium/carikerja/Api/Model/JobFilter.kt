package lab.codetorium.carikerja.Api.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class JobFilter: Serializable {
    @SerializedName("name")
    var name: String = ""
    @SerializedName("value")
    var value: String = ""
    @SerializedName("input_value")
    var valueType: String = ""
    var hint: String? = ""
    @SerializedName("suggested_value")
    var suggestedValue: List<Any>? = listOf()
    var inputtedValue: String? = null
}