package lab.codetorium.carikerja.Api.Service

import lab.codetorium.carikerja.Api.Api.Companion.OBJECT_PER_PAGE
import lab.codetorium.carikerja.Api.Model.Job
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface AvailableJobService {
    @GET("jobs")
    fun retrieveJobs(@Query("page") page: Int = 1,
                     @Query("per_page") perPage: Int = OBJECT_PER_PAGE): Call<ArrayList<Job>>

    @GET("jobs/{id}")
    fun retrieveJobs(id: Long)

    @GET("search_jobs")
    fun searchJob(@Query("title") title: String? = null,
                     @Query("field") field: String? = null,
                     @Query("position") position: String? = null,
                     @Query("is_bumn") isBumn: Boolean? = null,
                     @Query("page") page: Int = 1,
                     @Query("per_page") perPage: Int = OBJECT_PER_PAGE) : Call<ArrayList<Job>>
}